﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TasksBasic
{
    class t4
    {
        static void Main(string[] args)
        {
            Console.Write("Enter divide number: ");
            int firstNum = int.Parse(Console.ReadLine());
            int num = 0;
            bool isAllDivided = true;

            while (true)
            {
                Console.Write("Enter number: ");
                num = int.Parse(Console.ReadLine());

                if (num == -1)
                {
                    break;
                }

                if(num % firstNum != 0 && isAllDivided)
                {
                    isAllDivided = false;
                }
            }

            Console.Write("/n" + isAllDivided);

        }
    }
}
