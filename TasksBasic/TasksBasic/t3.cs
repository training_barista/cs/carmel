using System;

namespace TrainCode
{
    class t3
    {
        static void Main(string[] args)
        {
            PrintNumbers();
        }

        static void PrintNumbers()
        {
            for (int i = 1; i <= 1000; i++)
            {
                if (i % 5 == 0 && i % 3 == 0)
                {
                    Console.WriteLine("FizzBuzz");
                }
                else if (i % 3 == 0)
                {
                    Console.WriteLine("Fizz");
                } 
                else if (i % 5 == 0)
                {
                    Console.WriteLine("Buzz");
                }
                else 
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}