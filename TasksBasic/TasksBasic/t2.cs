﻿using System;

namespace TrainCode
{
    class t2
    {
        static void Main(string[] args)
        {
            int grade = 0, num = 0, sum = 0, numFalied = 0, max = 0, min = 100;

            while (true)
            {
                Console.WriteLine("Enter grade: ");
                grade = int.Parse(Console.ReadLine());

                if (grade == -1)
                {
                    break;
                }

                num++;

                sum += grade;

                if (grade > max)
                {
                    max = grade;
                }

                if (grade < min)
                {
                    min = grade;
                }

                if (grade < 60)
                {
                    numFalied++;
                }
            }

            Console.WriteLine("The highest garde is " + max);
            Console.WriteLine("The lowest garde is " + min);
            Console.WriteLine("The avarage " + (float)sum / num);
            Console.WriteLine("The number of falied grades is " + numFalied);
        }
    }
}
