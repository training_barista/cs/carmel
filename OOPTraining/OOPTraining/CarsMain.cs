﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPTraining
{
    class CarsMain
    {
        static void Main(string[] args)
        {
            CarsField f = new CarsField();
            Car a = new HybridCar(2020, 100000, 200000, "toyota", "white");
            Car b = new ElectricCar(2004, 250000, 120000, "skoda", "silver");
            Car c = new GasCar(2011, 400000, 150000, "kia", "red");
            f.AddCar(a);
            f.AddCar(b);
            f.AddCar(c);

            f.printCarsDetails();
            f.printCarsFromColor();
            f.printCarsFromType();
            f.printCarsFromYear();

        }
    }

    abstract class Car
    {
        public string type;
        public int price;
        public int year;
        public int km;
        public string color;
        public string company;


        public virtual void printType()
        {
            Console.WriteLine("type - " + type);
        }

        public virtual void printCarDetails()
        {
            this.printType();
            Console.WriteLine("price - " + this.price);
            Console.WriteLine("km - " + this.km);
            Console.WriteLine("color - " + this.color);
            Console.WriteLine("company - " + this.company);
            Console.WriteLine("year - " + this.year);
            Console.WriteLine();
        }
    }

    class ElectricCar : Car
    {

        public ElectricCar(int year, int km, int price, string company, string color) 
        {
            this.price = price;
            this.km = km;
            this.year = year;
            this.company = company;
            this.color = color;
            this.type = "electric";
        }
    }

    class GasCar : Car
    {
        public GasCar(int year, int km, int price, string company, string color)
        {
            this.price = price;
            this.km = km;
            this.year = year;
            this.company = company;
            this.color = color;
            this.type = "gas";
        }
    }

    class HybridCar : Car
    {
        public HybridCar(int year, int km, int price, string company, string color)
        {
            this.price = price;
            this.km = km;
            this.year = year;
            this.company = company;
            this.color = color;
            this.type = "hybrid";
        }
    }

    class CarsField
        {
            private List<Car> cars;

            public CarsField()
            {
                cars = new List<Car>();
            }

            public void AddCar(Car car)
            {
                cars.Add(car);
            }

            public void RemoveCar(Car car)
            {
                cars.Remove(car);
            }

            public void printCarsDetails()
            {
                foreach (Car car in this.cars)
                {
                    Console.WriteLine("car " + cars.IndexOf(car) + " :");
                    car.printCarDetails();
                }
            }

            public void printCarsFromType()
            {
                Console.Write("Enter type car: electric / hybrid / gas? ");
                string type = Console.ReadLine();

                foreach (Car car in this.cars)
                {
                    if (string.Equals(car.type, type))
                    {
                        car.printCarDetails();
                    }
                }
            }

            public void printCarsFromColor()
            {
                Console.Write("Enter car color: ");
                string color = Console.ReadLine();

                foreach (Car car in this.cars)
                {
                    if (string.Equals(car.color, color))
                    {
                        car.printCarDetails();
                    }
                }
            }

            public void printCarsFromYear()
            {
                Console.Write("Enter min year: ");
                int year = int.Parse(Console.ReadLine());

                foreach (Car car in this.cars)
                {
                    if (car.year > year)
                    {
                        car.printCarDetails();
                    }
                }
            }

    }
}
