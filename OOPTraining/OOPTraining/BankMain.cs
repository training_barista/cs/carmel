﻿using System;

namespace OOPTraining
{
    class BankMain
    {
        static void Main(string[] args)
        {
            BankAccount account = new BankAccount();
            double interest = 1.03;
            int input = 0;
            int money = 0;

            do
            {
                Console.Write("\n0 - Close account\n" +
                              "1 - Deposit money\n" +
                              "2 - Withdrawal money\n" +
                              "3 - Next year\n" +
                              "Choose an option: ");
                input = int.Parse(Console.ReadLine());

                switch (input)
                {
                    case 1:
                        Console.Write("Enter deposit number: ");
                        money = int.Parse(Console.ReadLine());
                        account.Deposit(money);
                        break;
                    case 2:
                        Console.Write("Enter withdrawal number: ");
                        money = int.Parse(Console.ReadLine());
                        account.Withdrawal(money);
                        break;
                    case 3:
                        interest = account.AddInterest(interest);
                        break;
                }
                Console.WriteLine("Money in the account : " + account.Money);

            } while (input != 0);
        }
    }

    class BankAccount
    {
        private double money;

        public BankAccount()
        {
            this.money = 0;
        }

        public double Money { get => money; set => money = value; }

        public void Deposit(double number)
        {
            this.money += number;
        }
        public void Withdrawal(double number)
        {
            if (this.money - number > 0)
            {
                this.money -= number;
            }
            else
            {
                Console.WriteLine("You do not have enough money :(");
            }
        }

        public double AddInterest(double interest)
        {
            Random rnd = new Random();
            int increase = rnd.Next(-1, 3);
            if (increase == 0) { return interest; }
            interest += increase / 100.0;
            this.money = this.money * interest;
            return interest;
        }
    }
}
