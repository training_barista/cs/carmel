﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPTraining
{
    class MatrixMain
    {
        static void Main(string[] args)
        {
            int[,] a = new int[2, 2] { { 7, 9 }, { 3, 5 } };
            int[,] b = new int[2, 2] { { 1, 3 }, { 7, 0 } };
            Matrix a1 = new Matrix(a);
            Matrix b1 = new Matrix(b);
            Matrix c1 = a1 + b1;
            Console.WriteLine(c1.GetValueAt(1, 1));
        }


    }

    class Matrix
    {
        public int[,] matrix;

        public Matrix(int row, int column)
        {
            this.matrix = new int[row, column];
        }

        public Matrix(int[,] arr)
        {
            this.matrix = new int[arr.GetLength(0), arr.GetLength(0)];
            for (int row = 0; row < arr.GetLength(0); row++)
            {
                for (int column = 0; column < arr.GetLength(1); column++)
                {
                    matrix[row, column] = arr[row, column];
                }
            }
        }

        public int GetValueAt(int row, int column)
        {
            return this.matrix[row, column];
        }

        public int SumRow(int row)
        {
            int sum = 0;
            for (int column = 0; column < this.matrix.GetLength(1); column++)
            {
                sum += this.matrix[row, column];
            }
            return sum;
        }

        public int SumColumn(int column)
        {
            int sum = 0;
            for (int row = 0; row < this.matrix.GetLength(0); row++)
            {
                sum += this.matrix[row, column];
            }
            return sum;
        }

        public static Matrix operator +(Matrix b, Matrix c)
        {
            int rows = b.matrix.GetLength(0);
            int columns = b.matrix.GetLength(1);
            Matrix nemMatrix = new Matrix(rows, columns);
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    nemMatrix.matrix[row, column] = b.GetValueAt(row, column) + c.GetValueAt(row, column);
                }
            }
            return nemMatrix;
        }

        public static Matrix operator -(Matrix b, Matrix c)
        {
            int rows = b.matrix.GetLength(0);
            int columns = b.matrix.GetLength(1);
            Matrix nemMatrix = new Matrix(rows, columns);
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    nemMatrix.matrix[row, column] = b.GetValueAt(row, column) - c.GetValueAt(row, column);
                }
            }
            return nemMatrix;
        }

        public static Matrix operator *(Matrix b, Matrix c)
        {
            int rows = b.matrix.GetLength(0);
            int columns = b.matrix.GetLength(1);
            Matrix nemMatrix = new Matrix(rows, columns);
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    nemMatrix.matrix[row, column] = b.GetValueAt(row, column) * c.GetValueAt(row, column);
                }
            }
            return nemMatrix;
        }
    }
}
