﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOPTraining
{
    class SongContest
    {
        static void Main(string[] args)
        {
            //t1
            Song s1 = new Song("aaa1", "aaaaaaaaa1");
            Song s2 = new Song("aaa2", "aaaaaaaaa2");

            Singer ladyGaga = new Singer("Lady Gaga", s1);
            Singer christinaAguilera = new Singer("Christina Aguilera", s2);

            Judge simon = new Judge("Simon Phillip Cowell");

            /*ladyGaga.SingSong();
            christinaAguilera.SingSong();

            simon.singerWinner(ladyGaga, christinaAguilera).name;*/

            //t2
            Song s3 = new Song("aaa3", "aaaaaaaaa3");
            Song s4 = new Song("aaa4", "aaaaaaaaa4");
            Song s5 = new Song("aaa5", "aaaaaaaaa5");

            Singer singer3 = new Singer("singer3", s5);
            Singer singer1 = new Singer("singer1", s3);
            Singer singer2 = new Singer("singer2", s4);

            Contest contest = new Contest(new List<Competitor>() { ladyGaga, christinaAguilera, singer1, singer2, singer3}, simon);
            //contest.cotestWinner();

            //t3
            List<Song> songs = new List<Song>() { s1, s2, s3 };
            /*Singer singer4 = new Singer("singer4", songs);
            Singer singer5 = new Singer("singer5", songs);
            Singer singer6 = new Singer("singer6", songs);
            Singer singer7 = new Singer("singer7", songs);
            Singer singer8 = new Singer("singer8", songs);
            Singer singer9 = new Singer("singer9", songs);

            Contest contest3 = new Contest(new List<Competitor>() { singer4, singer5, singer6, singer7, singer8, singer9 }, 3);
            contest3.cotestWinnerByViewers();*/

            //t5
            Musician musician1 = new Musician("m1", "guitar");
            Musician musician2 = new Musician("m2", "drummer");

            Band b = new Band("PATD", s1, new List<Singer>() { ladyGaga }, new List<Musician>() { musician1, musician2 });
            Contest contest4 = new Contest(new List<Competitor>() { christinaAguilera, b }, simon);

            contest4.cotestWinner();
        }

        class Contest
        {
            public List<Competitor> competitors;
            public Judge judge;
            public List<Viewer> viewers;


            public Contest(List<Competitor> competitors, Judge judge)
            {
                this.competitors = new List<Competitor>(competitors);
                this.judge = new Judge(judge.name);
            }

            public Contest(List<Competitor> competitors, int numViewers)
            {
                this.competitors = new List<Competitor>(competitors);
                this.viewers = new List<Viewer>();
                for (int i = 0; i < numViewers; i++)
                {
                    this.viewers.Add(new Viewer("v" + i.ToString()));
                }
            }

            public Competitor cotestWinner()
            {
                if (this.judge == null)
                {
                    return cotestWinnerByViewers();
                }
                return cotestWinnerByJudge();
            }

            public Competitor cotestWinnerByJudge()
            {
                List<Competitor> competitors = new List<Competitor>(this.competitors);
                while (competitors.Count != 1)
                {
                    if (competitors.Count % 2 != 0)
                    {
                        Duel(competitors[0], competitors[1]);
                        competitors.Remove(this.judge.singerLoser(competitors[0], competitors[1]));
                    }

                    List<Competitor> winners = new List<Competitor>();
                    int len = competitors.Count;
                    for (int i = 0; i < len; i += 2)
                    {
                        Duel(competitors[i], competitors[i + 1]);
                        winners.Add(this.judge.singerWinner(competitors[i], competitors[i + 1]));
                    }
                    competitors = new List<Competitor>(winners);

                }
                Console.WriteLine("\n\nTHE WINNER OF THE COMPETION IS " + competitors[0].name + "!! :-) :-)");
                return competitors[0];
            }

            public void Duel(Competitor s1, Competitor s2)
            {
                Console.WriteLine("\n---The competitors are " + s1.name + " and " + s2.name + "---");
                s1.PerformSong();
                s2.PerformSong();
            }

            public Competitor cotestWinnerByViewers()
            {
                foreach (Competitor s in this.competitors)
                {
                    s.PerformSong();
                }
                
                List<int> wins = new List<int>(new int[this.competitors.Count]);
                foreach (Viewer viewer in this.viewers)
                {
                    wins[SingerIndex(viewer.ChooseWinner(this.competitors))]++;
                }

                int max = 0;
                int indexMax = 0;
                for (int i = 0; i < wins.Count; i++)
                {
                    if (wins[i] > max)
                    {
                        max = wins[i];
                        indexMax = i;
                    }
                }
                Console.WriteLine("\n\nTHE WINNER OF THE COMPETION IS " + this.competitors[indexMax].name + "!! :-) :-)");
                return this.competitors[indexMax];
            }

            public int SingerIndex(Competitor singer)
            {
                foreach (Competitor singer1 in this.competitors)
                {
                    if (singer1.name == singer.name)
                    {
                        return this.competitors.IndexOf(singer1);
                    }
                }
                return -1;
            }
        }

        class Viewer
        {
            public string name;

            public Viewer(string name)
            {
                this.name = name;
            }

            public Competitor ChooseWinner(List<Competitor> competitors)
            {
                Random random = new Random();
                return competitors[random.Next(0, competitors.Count)];
            }
        }

        class Judge
        {
            public string name;

            public Judge (string name)
            {
                this.name = name;    
            }
            
            public Competitor singerWinner(Competitor s1, Competitor s2)
            {
                Random rnd = new Random();
                int num1 = s1.song.name.Length + s1.song.lyrics.Length + s1.name.Length / 2;
                int num2 = s2.song.name.Length + s2.song.lyrics.Length + s2.name.Length / 2;
                int rndNum = rnd.Next(-num1, num1);
                if (num1 + rndNum > num2)
                {
                    Console.WriteLine("The winner is " + s1.name + "!! :)");
                    Console.WriteLine("The Loser is " + s2.name + " :( ");
                    return s1;
                }
                Console.WriteLine("The winner is " + s2.name + "!! :)");
                Console.WriteLine("The Loser is " + s1.name + " :( ");
                return s2;
            }

            public Competitor singerLoser(Competitor s1, Competitor s2)
            {
                if (this.singerWinner(s1, s2) == s1)
                {
                    return s2;
                }
                return s1;
            }
        }

        class Song
        {
            public string name;
            public string lyrics;

            public Song (string name, string lyrics)
            {
                this.name = name;
                this.lyrics = lyrics;
            }
        }

        class Competitor
        {
            public string name;
            public Song song;

            public Competitor(string name, Song song)
            {
                this.name = name;
                this.song = song;
            }

            public Competitor(string name, List<Song> songs)
            {
                this.name = name;
                Random random = new Random();
                this.song = songs[random.Next(0, songs.Count)];
                Console.WriteLine(this.name + " choose " + this.song.name + " song");
            }
                
            public virtual void PerformSong()
            {
                Console.WriteLine(this.song.lyrics);
            }
        }

        class Singer : Competitor
        {
            public Singer(string name, Song song) : base(name, song)
            {
            }

            public Singer(string name, List<Song> songs) : base(name, songs)
            {
            }
        }

        class Band : Competitor
        {
            public List<Singer> singers;
            public List<Musician> musicians;
            public Band(string name, Song song, List<Singer> singers, List<Musician> musicians) : base(name, song)
            {
                bool isValidLength = false;
                bool isValidGuitar = false;
                bool isValidDrummer = false;
                if (singers.Count > 0)
                {
                    isValidLength = true;
                }

                foreach (Musician musician in musicians)
                {
                    if (musician.instrument == "guitar")
                    {
                        isValidGuitar = true;
                    }
                    if (musician.instrument == "drummer")
                    {
                        isValidDrummer = true;
                    }
                }

                if (!isValidLength && !isValidGuitar && !isValidDrummer)
                {
                    Console.WriteLine("The band is not valid \nevery band should has at least \none singer 1 guitar player \n1 drummer player");
                }
                else
                {
                    this.musicians = new List<Musician>(musicians);
                    this.singers = new List<Singer>(singers);
                }
            }

            public override void PerformSong()
            {
                foreach (Singer s in this.singers)
                {
                    s.PerformSong();
                }

                foreach (Musician m in this.musicians)
                {
                    m.PlaySong();
                }
            }
        }

        class Musician
        {
            public string name;
            public string instrument;

            public Musician (string name, string instrument)
            {
                this.name = name;
                this.instrument = instrument;
            }

            public void PlaySong()
            {
                Console.WriteLine("♫♫♫");
            }
        }

    }
}
