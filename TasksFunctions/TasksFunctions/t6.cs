﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TasksFunctions
{
    class t6
    {
        static void Main(string[] args)
        {
            string str = "A man, a plan, a canal: Panama";
            if (isStringPalindrome(str))
            {
                Console.WriteLine(str + " is palindrome");
            }
            else
            {
                Console.WriteLine(str + " isn't palindrome");
            }
        }

        static bool isStringPalindrome(string str)
        {
            str = cleanString(str);
            int len = str.Length;
            for (int i = 0; i < len / 2; i++)
            {
                if (str[i] != str[len - i - 1])
                {
                    return false;
                }
            }
            return true;
        }

        static string cleanString(string str)
        {
            str = str.ToLower();
            string cleanStr = "";
            foreach (int i in str)
            {
                if (i >= 'a' && i <= 'z')
                {
                    cleanStr += (char)i;
                }
            }
            return cleanStr;
        }
    }
}
