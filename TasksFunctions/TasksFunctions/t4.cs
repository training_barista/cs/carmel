﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TasksFunctions
{
    class t4
    {
        static void Main(string[] args)
        {
            int[] arr = { 1, 2, 5, 6, 3, 9, 8, 4, 7 };
            Console.WriteLine("[{0}]", string.Join(", ", sortArr(true, arr)));
            Console.WriteLine("[{0}]", string.Join(", ", sortArr(false, arr)));        }

        static int[] sortArr(bool isSortUpwards, params int[] arr)
        {
            int temp;
            for (int j = 0; j <= arr.Length - 2; j++)
            {
                for (int i = 0; i <= arr.Length - 2; i++)
                {
                    if (isSortUpwards && arr[i] > arr[i + 1])
                    {
                        temp = arr[i + 1];
                        arr[i + 1] = arr[i];
                        arr[i] = temp;
                    }
                    else if (!isSortUpwards && arr[i] < arr[i + 1])
                    {
                        temp = arr[i + 1];
                        arr[i + 1] = arr[i];
                        arr[i] = temp;
                    }
                }
            }
            return arr;
        }

    }
}
