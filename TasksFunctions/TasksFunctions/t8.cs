﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TasksFunctions
{
    class t8
    {
        static void Main(string[] args)
        {
            int[, ] arr = new int[,] { { 0, 1, 0 }, { 0, 0, 1 }, { 1, 1, 1 }, { 0, 0, 0 } };

            arr = NextStateBoard(arr);

            int columns = arr.GetLength(1);
            int rows = arr.GetLength(0);

            Console.WriteLine("New Arr: ");

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < columns; col++)
                    Console.Write(arr[row, col] + " - ");
                Console.WriteLine();
            }
        }

        static int[, ] NextStateBoard(int[,] arr)
        {
            int rowsCount = arr.GetLength(0);
            int columnsCount = arr.GetLength(1);

            int[,] newArr = new int[rowsCount, columnsCount];

            for (int row = 0; row < rowsCount; row++)
            {
                for (int column = 0; column < columnsCount; column++)
                {
                    int neighbors = LivingNeighbors(row, column, arr);
                    if (neighbors < 2 && arr[row, column] == 1)
                    {
                        newArr[row, column] = 0;
                    }
                    else if (neighbors == 3 && arr[row, column] == 0)
                    {
                        newArr[row, column] = 1;
                    }
                    else if (neighbors > 3 && arr[row, column] == 1)
                    {
                        newArr[row, column] = 0;
                    }
                    else
                    {
                        newArr[row, column] = arr[row, column];
                    }
                }
            }
            return newArr;
        }

        static int LivingNeighbors(int row, int column, int[,] arr)
        {
            int neighbors = 0;
            int rowCount = arr.GetLength(0) - 1;
            int columnCount = arr.GetLength(1) - 1;

            //Check if neighbors in the same column are alive.
            if (rowCount > row && arr[row + 1, column] == 1)
            {
                neighbors++;
            }
            if (0 < row && arr[row - 1, column] == 1)
            {
                neighbors++;
            }

            //Check if neighbors in the same row are alive.
            if (columnCount > column && arr[row, column + 1] == 1)
            {
                neighbors++;
            }
            if (0 < column && arr[row, column - 1] == 1)
            {
                neighbors++;
            }

            //Check if neighbors in the same diagonal are alive.
            if (columnCount > column && rowCount > row && arr[row + 1, column + 1] == 1)
            {
                neighbors++;
            }
            if (0 < column && 0 < row && arr[row - 1, column - 1] == 1)
            {
                neighbors++;
            }

            if (columnCount > column && 0 < row && arr[row - 1, column + 1] == 1)
            {
                neighbors++;
            }
            if (0 < column && rowCount > row && arr[row + 1, column - 1] == 1)
            {
                neighbors++;
            }

            return neighbors;
        }
    }
}
