﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TasksFunctions
{
    class t9
    {
        static void Main(string[] args)
        {
            Console.WriteLine(FindNumber());
        }

        static int FindNumber()
        {
            int number = 2;
            int jumpNumbers = 0;

            while (jumpNumbers != number * 0.99)
            {
                number++;
                if (!IsNumberGoingDown(number) && !IsNumberGoingUp(number))
                {
                    jumpNumbers++;
                }
            }
            return number;
        }

        static bool IsNumberGoingUp(int number)
        {
            int digit = 0;
            while (number >= 10)
            {
                digit = number % 10;
                number /= 10;
                if (digit < number % 10)
                {
                    return false;
                }
            }
            return true;
        }

        static bool IsNumberGoingDown(int number)
        {
            int digit = 0;
            while(number >= 10)
            {
                digit = number % 10;
                number /= 10;
                if (digit > number % 10)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
