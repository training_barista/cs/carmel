﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TasksFunctions
{
    class t3
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------rock paper scissors GAME------");
            
            Console.Write("player one: rock/paper/scissors?");
            string player1 = Console.ReadLine();

            Console.Clear();

            Console.Write("player one: rock/paper/scissors?");
            string player2 = Console.ReadLine();

            if (player1 == player2)
            {
                Console.WriteLine("It's a tie!");
            }
            else if (player1 == "rock" && player2 == "scissors" ||
                     player1 == "paper" && player2 == "rock" ||
                     player1 == "scissors" && player2 == "paper")
            {
                Console.WriteLine("First player wins!");
            }
            else
            {
                Console.WriteLine("Second player wins!");
            }
                       
        }
    }
}
