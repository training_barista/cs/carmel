﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TasksFunctions
{
    class t2
    {
        static void Main(string[] args)
        {
            Console.Write("MathProgram: ");
            string input = Console.ReadLine();

            while (input.Length != 3)
            {
                Console.WriteLine("The input needs to be 3 chars long.");
                Console.Write("MathProgram: ");
                input = Console.ReadLine();
            }

            int firstNum = input[0] - '0';
            char op = input[1];
            int secondNum = input[2] - '0';

            int answer = 0;

            switch (op)
            {
                case '+':
                    answer = firstNum + secondNum;
                    break;
                case '-':
                    answer = firstNum - secondNum;
                    break;
                case '*':
                    answer = firstNum * secondNum;
                    break;
                case '/':
                    answer = firstNum / secondNum;
                    break;
                case '^':
                    answer = (int)Math.Pow(firstNum, secondNum);
                    break;
                default:
                    Console.WriteLine("There are only + - / * ^ options.");
                    break;
            }

            Console.WriteLine(" -> " + answer);
        }
    }
}
