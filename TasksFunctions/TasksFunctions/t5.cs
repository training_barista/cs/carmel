﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TasksFunctions
{
    class t5
    {
        static void Main(string[] args)
        {
            char[] letters = { 'z', 'p' };
            string str = "zpple";
            ShiftLetters(ref str, letters);
            Console.WriteLine("str -> " + str);
        }

        static void ShiftLetters(ref string str, params char[] letters)
        {
            foreach (char letter in letters)
            {
                int inx = str.IndexOf(letter);
                if (inx != -1)
                {
                    str = str.Replace(letter, AddToChar(letter));
                }

            }
        }

        static char AddToChar(char letter)
        {
            if (letter + 10 > 'z')
            {
                letter = (char)(('z' - letter) + 'a' + 9);
            } 
            else
            {
                letter = (char)(letter + 10);
            }
            return letter;
        }
    }
}
