﻿using System;
namespace TasksFunctions
{
    class t1
    {
        static void Main(string[] args)
        {
            Console.Write("Enter max number: ");
            int max = int.Parse(Console.ReadLine());

            Console.Write("Enter min number: ");
            int min = int.Parse(Console.ReadLine());

            while(min > max)
            {
                Console.WriteLine("The min number needs to be smaller the the max number.");
                Console.Write("Enter min number: ");
                min = int.Parse(Console.ReadLine());
            }

            int num = 0;
            CalcRandomNum(max, min, ref num);

            Console.Write("Random number: " + num);
        }

        public static void CalcRandomNum(int max, int min, ref int num)
        {
            Random random = new Random();
            num = random.Next(min, max + 1);
        }
    }
}
