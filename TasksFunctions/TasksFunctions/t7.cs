﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TasksFunctions
{
    class t7
    {
        static void Main(string[] args)
        {
            int num = FindSingleNmber(new int[]{ 4, 1, 2, 1, 2});
            Console.WriteLine(num);
        }

        static int FindSingleNmber(int[] arr)
        {
            bool isSingleNum = true;
            for (int j = 0; j < arr.Length; j++)
            {
                isSingleNum = true;
                for (int i = 0; i < arr.Length; i++)
                {
                    if (arr[i] == arr[j] && i != j)
                    {
                        isSingleNum = false;
                        break;
                    }
                }
                if (isSingleNum)
                {
                    return arr[j];
                }
            }
            return 0;
        }
    }
}
